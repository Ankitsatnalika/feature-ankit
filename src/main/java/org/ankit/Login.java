package org.ankit;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Login {

    WebDriver driver;
    ObjectLocator locator = new ObjectLocator();
    Logger log = Logger.getLogger(Login.class);


    @Given("I navigate to the landing URL")
    public void INavigateToURL()
    {
        System.setProperty("webdriver.chrome.driver","C:\\\\Users\\\\satnalika\\\\Downloads\\chromedriver.exe");
        driver = new ChromeDriver();
        log.info("*************************************Starting Browser************************");
        driver.manage().window().maximize();
        driver.get("https://www.flipkart.com/");
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
    }


    @When("^I enter \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iEnterAnd(String Username, String Password) throws Exception {
        driver.findElement(By.xpath(locator.Elementlocator("txt_username"))).sendKeys(Username);
        driver.findElement(By.xpath(locator.Elementlocator("txt_password"))).sendKeys(Password);
        driver.findElement(By.xpath(locator.Elementlocator("btn_Login"))).click();
    }

    @And("^I logout from the application$")
    public void iLogoutFromTheApplication() throws IOException {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(By.xpath(locator.Elementlocator("lnk_Ankit")))).build().perform();
        driver.findElement(By.xpath(locator.Elementlocator("lnk_logout"))).click();
        driver.quit();

    }
}
